from django.urls import path
from . import views  # importing everything from the views.py where the needed function is

urlpatterns = [
    path("products", views.products, name='products'),  # calling to the function created in Posts\views.py
    # path("aboutus", views.about_us_page, name='aboutus')
]