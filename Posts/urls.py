from django.urls import path
from . import views  # importing everything from the views.py where the needed function is
# or from .views import *   # this way using views.something is not needed always

urlpatterns = [
    # path("", views.home_page_view, name='home'),  # calling to the function created in Posts\views.py
    # path("aboutus", views.about_us_page, name='aboutus')  # functional based
    path("", views.HomeView.as_view(), name='home'),   # need to add path views.Homeview, class based
    path("post/<int:pk>/", views.PostDetailView.as_view(), name='post_detail'),  # for class based comment add
    # path("post/<int:pk>/", views.post_detail, name='post_detail'),  # for function based comment add
    path("post/new/", views.PostCreateView.as_view(), name='post_create'),
    path('post/<int:pk>/edit/', views.PostUpdateView.as_view(), name='post_update'),
    path('post/<int:pk>/delete/', views.PostDeleteView.as_view(), name='post_delete'),
]




