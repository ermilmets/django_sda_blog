from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.name}"


class Posts(models.Model):   # recommended is singular class name
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateField()
    categories = models.ForeignKey(Category, related_name='posts', on_delete=models.CASCADE, null=True, blank=True) # dont know

    def __str__(self):   #necessary? for admin site visual preferences, recommended
        return f"{self.title} by {self.author}"


class Comment(models.Model):
    post = models.ForeignKey(Posts, on_delete=models.CASCADE, related_name='comments')
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    date_posted = models.DateField(auto_now_add=True)  # automatically adds date

    def __str__(self):
        return f"Comment by {self.author.username} on {self.post.title}"


class FeaturedPost(models.Model):
    post = models.OneToOneField(Posts, on_delete=models.CASCADE)  # Foreign Key for the class Posts
    priority = models.IntegerField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"Featured Post (inside models.py): {self.post.title}"






