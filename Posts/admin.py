from django.contrib import admin
from .models import Posts, Category, Comment, FeaturedPost  # importing the class Posts

# Register your models here.   otherwise wont work

admin.site.register(Posts)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(FeaturedPost)



