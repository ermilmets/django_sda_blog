import pdb

from django.shortcuts import render, redirect, get_object_or_404
from .models import Posts, FeaturedPost
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .forms import CommentForm, PostForm
from django.urls import reverse_lazy


# Create your views here.
def home_page_view(request):
    # Sample blog data
    # posts = [
    #     {
    #         'title': 'First Blog Post',
    #         'author': 'John Doe',
    #         'content': 'This is the content of the first blog post.',
    #         'date_posted': 'June 1, 2024'
    #     },
    #     {
    #         'title': 'Second Blog Post',
    #         'author': 'Jane Smith',
    #         'content': 'This is the content of the second blog post.',
    #         'date_posted': 'June 2, 2024'
    #     },
    #     {
    #         'title': 'Third Blog Post',
    #         'author': 'John Snow',
    #         'content': 'This is the content of the third blog post.',
    #         'date_posted': 'June 3, 2024'
    #     }
    #     # Add more blog entries here if needed
    # ]
    posts = Posts.objects.all()  # enables data from database
    posts = Posts.objects.order_by('-date_posted')

    featured_posts = FeaturedPost.objects.all().order_by('priority')

    context = {'posts': posts, 'featured_posts': featured_posts,
               'aboutus': 'Aboutus data inside views.py dictionary', 'contactus': 'Contactus data '
                                                                                                  'inside views.py '
                                                                                                  'dictionary'}
    return render(request, 'home.html', context)


# def about_us_page(request):
#     pass


# class based view
class HomeView(ListView):
    model = Posts  # refers to the database model created in models.py
    template_name = 'home.html'  # where the data will be ?
    context_object_name = 'posts'  # when referring to the database object/model 'Posts' (for loops in home.html page)
    ordering = ['title']  # descending order? ['-date_posted']  ['-title']

    # fetches the context defined above, adds to context whatever you define, can be used with django templates in html
    def get_context_data(self, *, object_list=None, **kwargs):      # default method, used when multiple giving data
        context = super().get_context_data(**kwargs)
        context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')  # adding featured posts, shell cmnd
        # print(f"Featured posts: {context['featured_posts']}")
        # FeaturedPost.objects.all().order_by('priority') # before ordering, doesn't work though, causes errors refer.
        # id = self.request.session['id']
        # id = self.request.session.get('id', 1)
        # self.request.session['id'] = 1
        visit_count = self.request.session.get('visit_counts', 0)
        self.request.session['visit_counts'] = visit_count + 1
        context['count'] = visit_count
        return context  # returning context data with featured posts added

class PostDetailView(DetailView):
    model = Posts
    template_name = "post/post_detail.html"
    # context_object_name = "posts"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comments'] = self.object.comments.all()

        return context

    def post(self, request, *args, **kwargs):
        # print(request.POST)   # for testing
        self.object = self.get_object()
        comment_form = CommentForm(request.POST)  # content from the CommentForm
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = self.object
            comment.save()

            return redirect('post_detail', pk=self.object.id)  # needs imported redirect
        context = self.get_context_data(object=self.object)
        context['comment_form'] = comment_form
        return self.render_to_response(context)


def post_detail(request, pk):
    # Retrieve the Post object
    post = get_object_or_404(Posts, pk=pk)
    # pdb.set_trace()  # for testing?
    user_id = request.session.get('id')  # can retrieve this session id in another place
    # Handle the POST  request
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        comment_form = CommentForm()

    # Context data for rendering the template
    context = {
        'post': post,
        'comments': post.comments.all(),
        'comment_form': comment_form
    }
    return render(request, 'post/post_detail.html', context)


class PostCreateView(CreateView):
    model = Posts
    form_class = PostForm
    template_name = 'post/post_form.html'  # 'post/post_form.html' needs to be
    success_url = reverse_lazy('post_list')    # reverse_lazy('home')

# class PostCreateView(CreateView):
#     model = Posts
#     form_class = PostForm
#     template_name = 'post/post_form.html'
#     success_url = reverse_lazy('home')

class PostUpdateView(UpdateView):
    model = Posts
    form_class = PostForm
    template_name = 'post/post_form.html'
    success_url = reverse_lazy('home')

class PostDeleteView(DeleteView):
    model = Posts
    template_name = 'post/post_confirm_delete.html'
    success_url = reverse_lazy('home')

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     post = self.get_object()
    #     context['post'] = post
    #     return context




# from django.shortcuts import render
#
# # Create your views here.
#
# posts = [
#     {
#         'title': 'First Blog Post',
#         'name': 'Something 1'
#     }
#
#
# ]
#
#
# context = {'posts': posts}
#
# def home_page_view(request):
#     return render(request, 'home.html', context)

