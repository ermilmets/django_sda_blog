from django import forms
from .models import Posts, Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']   # only content field from the .models class

class PostForm(forms.ModelForm):
    class Meta:
        model = Posts
        fields = ['title', 'author', 'content']   # only content field from the .models class



