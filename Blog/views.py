from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate


# Class based views

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, required=True, help_text='Required. Enter a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )



class ContactView(TemplateView):
    template_name = 'contact.html'

    def get(self, request,*args,**kwargs):
        return super().get(request, *args, **kwargs)


class AboutView(TemplateView):
    template_name = 'aboutus.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ProductsView(TemplateView):
    template_name = 'products.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)  # built-in django
        if form.is_valid():
            form.save()
            # username = form.cleaned_data.get('username')   # with SignupForm
            # raw_password = form.cleaned_data.get('password')
            # user = authenticate(username=username, password=raw_password)
            # login(request, user)   # logging in with given data
            # print("EVERYTHING IS CORRECT HERE")
            return redirect('login')   # 'home'
    else:
        form = SignUpForm()
    return render(request, "registration/signup.html", {'form': form})



# function based views:


def contact(request):
    # print(request.path)  # to check what the exact path is in the terminal when clicking a link = request
    return render(request, 'contact.html')


def about_us(request):
    return render(request, "aboutus.html")




